SCHEDULE="10 22 * * *"
# As the script creates hardlinks from content on data1, the below
# destintation should always be a path on /mnt/data1
YUMSNAPSHOT_DESTINATION="/mnt/data1/dist/tmp/ben/internal/yumsnapshot"
ADMIN_EMAIL="morrice@cern.ch"
