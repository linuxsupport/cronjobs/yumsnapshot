#!/bin/bash
#
# linuxsoft-sync-yumsnapshot
#
# script to take snapshot of yum repositories for given date.
#
# Jaroslaw.Polok@cern.ch
#

SNAP="all"
VERBOSE=1
CHECKSUM=0
NOACTION=0
FORCE=1
DATE=`/bin/date +%Y%m%d`
TODO="ai archive artifactselastic authz batch cc7 cernbox cuda cvmfs debian elbe elrepo els eos epel grafana hashicorp htcondor hw itmon mongodb monit multifactor neo4j nux openafs oracle rpmfusion sec sectools starnet suse ubi7 ubi8 ubi9 umd4 xrootd"
SPREF="/mnt/data1/dist"
DPREF=$YUMSNAPSHOT_DESTINATION

if [ -z $DPREF ]; then
  echo "DPREF variable cannot be null, exiting"
  exit 1
fi

if [ -z $ADMIN_EMAIL ]; then
  ADMIN_EMAIL="lxsoft-admins@cern.ch"
fi

function do_admin_email {
  SUBJECT=$1
  BODY=$2
  MAILMX=cernmx.cern.ch
  FROM="Linux.Support@cern.ch"

  MAILTMP=`/bin/mktemp /tmp/$DIST$$.XXXXXXXX`
  echo "To: $ADMIN_EMAIL" >> $MAILTMP
  echo "From: $FROM" >> $MAILTMP
  echo "Reply-To: noreply.Linux.Support@cern.ch" >> $MAILTMP
  echo "Return-Path: $ADMIN_EMAIL" >> $MAILTMP
  echo "Subject: $SUBJECT" >> $MAILTMP

  echo -e "\nDear admins,\n" >> $MAILTMP
  # ensure shell globbing doesn't mess up our email
  set -f
  echo -e $BODY >> $MAILTMP
  set +f
  echo -e "\n---\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)" >> $MAILTMP

  cat $MAILTMP | swaks --server $MAILMX --to $ADMIN_EMAIL --from $FROM --helo cern.ch --data -
  rm -f $MAILTMP
}

function help {
  echo "`basename $0` [ -v ][ -f ][ -n ] [ -d YYYYMMDD ] [ -s ARG ]"
  echo "          -f = force the run"
  echo "          -v = verbose"
  echo "          -n = noaction"
  echo "          -d YYYYMMDD - run for given date"
  echo "          -s ARG , where ARG = $TODO"
  exit 0
}

while getopts ":vhs:nfd:" flag; do
  [ "$flag" == "v" ] && VERBOSE=1
  [ "$flag" == "h" ] && help
  [ "$flag" == "s" ] && SNAP=$OPTARG
  [ "$flag" == "n" ] && NOACTION=1
  [ "$flag" == "f" ] && FORCE=1
  [ "$flag" == "d" ] && DATE=$OPTARG
done

[ "$SNAP" != "all" ] && TODO=$SNAP

for COMP in ${TODO}; do

  case "$COMP" in
  "cc7")
    TOPREPO="cern/centos/7/"
    TOPFILE=$TOPREPO
    TOPDEST=$TOPREPO
    REPOS="."
    RPREF=""
    ARCHS="centosplus/x86_64 centosplus-testing/x86_64 cern/x86_64 cern-testing/x86_64 cernonly/x86_64 cernonly-testing/x86_64 extras/x86_64 extras-testing/x86_64 os/x86_64 updates/x86_64 updates-testing/x86_64 cr-testing/x86_64 cr/x86_64 fasttrack-testing/x86_64 fasttrack/x86_64 sclo/x86_64 rt/x86_64"
    SUFF=""
    PROT=""
    ;;

  "els")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="els/"
    ARCHS="els7-stable/x86_64/os els7-stable/x86_64/debug"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "elbe")
    TOPREPO="mirror/debian.linutronix.de/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/debian.linutronix.de/"
    ARCHS="elbe"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;
    
  "elrepo")
    TOPREPO="elrepo/"
    TOPFILE=$TOPREPO
    TOPDEST="elrepo/"
    ARCHS="elrepo/el9 kernel/el9 dud/el9 extras/el9 testing/el9"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "archive")
    TOPREPO="internal/archive/"
    TOPFILE=$TOPREPO
    TOPDEST="archive/"
    ARCHS="epel/7/x86_64 epel/7/source/tree"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "epel")
    TOPREPO="epel/"
    TOPFILE=$TOPREPO
    TOPDEST="epel/"
    ARCHS="7/x86_64 8/Everything/x86_64 8/Everything/aarch64 8/Modular/x86_64 8/Modular/aarch64 next/8/Everything/x86_64 next/8/Everything/aarch64 9/Everything/aarch64 9/Everything/x86_64 next/9/Everything/aarch64 next/9/Everything/x86_64 testing/9/Everything/aarch64 testing/9/Everything/x86_64 testing/next/9/Everything/x86_64 testing/next/9/Everything/aarch64 testing/9/Everything/source 7/source/tree 8/Everything/source/tree 9/Everything/source/tree"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "ai")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="ai/"
    ARCHS="ai7-stable/x86_64/debug"
    ARCHS="$ARCHS ai7-stable/x86_64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "sec")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="sec/"
    ARCHS="sec7-stable/x86_64/os sec7-stable/x86_64/debug"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "sectools")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="sectools/"
    ARCHS="sectools7-stable/x86_64/debug sectools7-stable/x86_64/os"
    ARCHS="$ARCHS sectools8s-stable/x86_64/debug sectools8s-stable/x86_64/os sectools8s-stable/aarch64/debug sectools8s-stable/aarch64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "multifactor")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="multifactor/"
    ARCHS="multifactor7-stable/x86_64/debug multifactor7-stable/x86_64/os multifactor9-stable/x86_64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "hw")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="hw/"
    ARCHS="hw7-stable/x86_64/debug hw7-stable/x86_64/os"
    ARCHS="$ARCHS hw8s-stable/x86_64/debug hw8s-stable/x86_64/os hw8s-stable/aarch64/debug hw8s-stable/aarch64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "itmon")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="itmon/"
    ARCHS="itmon7-stable/x86_64/debug"
    ARCHS="$ARCHS itmon7-stable/x86_64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "batch")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="batch/"
    ARCHS="batch7-stable/x86_64/debug"
    ARCHS="$ARCHS batch7-stable/x86_64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "umd4")
    TOPREPO="mirror/repository.egi.eu/sw/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/repository.egi.eu/sw/"
    ARCHS="production/umd/4/centos7/x86_64/base production/umd/4/centos7/x86_64/updates"
    ARCHS="$ARCHS untested/umd/4/centos7/x86_64/"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "xrootd")
    TOPREPO="mirror/xrootd.cern.ch/sw/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/xrootd.cern.ch/sw/repos/"
    ARCHS="stable/slc/7/x86_64"
    ARCHS="$ARCHS testing/slc/7/x86_64"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "cvmfs")
    TOPREPO="mirror/cvmrepo.web.cern.ch/cvmrepo/yum/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/cvmrepo.web.cern.ch/cvmrepo/yum/"
    ARCHS="cvmfs/EL/7/x86_64"
    ARCHS="$ARCHS cvmfs-config/EL/7/x86_64"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "htcondor")
    TOPREPO="mirror/research.cs.wisc.edu/htcondor/yum/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/research.cs.wisc.edu/htcondor/yum/"
    ARCHS="development/rhel7/ stable/rhel7/"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "nux")
    TOPREPO="mirror/li.nux.ro/download/nux/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/li.nux.ro/download/nux/"
    ARCHS="dextop/el7/x86_64 dextop-testing/el7/x86_64"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "cernbox")
    TOPREPO="mirror/cernbox.cern.ch/cernbox/doc/Linux/repo/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/cernbox.cern.ch/cernbox/doc/Linux/repo/"
    ARCHS="CentOS_7/x86_64 CentOS_7/noarch"
    RPREF="../" # super ugly
    REPOS="."   # ugly
    SUFF=""
    PROT=""
    ;;

  "neo4j")
    TOPREPO="mirror/yum.neo4j.org/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/yum.neo4j.org/"
    ARCHS="stable"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "grafana")
    TOPREPO="mirror/packages.grafana.com/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/packages.grafana.com/"
    ARCHS="oss/rpm"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "oracle")
    TOPREPO="mirror/yum.oracle.com/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/yum.oracle.com/"
    ARCHS="repo/OracleLinux/OL7/developer/x86_64 repo/OracleLinux/OL7/oracle/instantclient/x86_64 repo/OracleLinux/OL8/developer/x86_64 repo/OracleLinux/OL8/oracle/instantclient/x86_64 repo/OracleLinux/OL9/developer/x86_64 repo/OracleLinux/OL9/oracle/instantclient/x86_64"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "suse")
    TOPREPO="mirror/download.opensuse.org/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/download.opensuse.org/"
    ARCHS="repositories/devel:/kubic:/libcontainers:/stable/CentOS_7/"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "rpmfusion")
    TOPREPO="mirror/download1.rpmfusion.org/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/download1.rpmfusion.org/"
    ARCHS="free/el/updates/8/x86_64 nonfree/el/updates/8/x86_64 free/el/updates/9/x86_64 nonfree/el/updates/9/x86_64"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "ubi7")
    TOPREPO="cdn-ubi.redhat.com/content/public/ubi/dist/ubi/server/7/7Server/"
    TOPFILE=$TOPREPO
    TOPDEST=$TOPREPO
    ARCHS="x86_64/os x86_64/extras/os x86_64/optional/os x86_64/devtools/1/os x86_64/rhscl/1/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "ubi8")
    TOPREPO="cdn-ubi.redhat.com/content/public/ubi/dist/ubi8/8/"
    TOPFILE=$TOPREPO
    TOPDEST=$TOPREPO
    ARCHS="x86_64/baseos/os x86_64/appstream/os x86_64/codeready-builder/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "ubi9")
    TOPREPO="cdn-ubi.redhat.com/content/public/ubi/dist/ubi9/9/"
    TOPFILE=$TOPREPO
    TOPDEST=$TOPREPO
    ARCHS="x86_64/baseos/os x86_64/baseos/debug x86_64/baseos/source x86_64/appstream/os x86_64/appstream/debug x86_64/appstream/source x86_64/codeready-builder/os x86_64/codeready-builder/debug x86_64/codeready-builder/source aarch64/baseos/os aarch64/baseos/debug aarch64/baseos/source aarch64/appstream/os aarch64/appstream/debug aarch64/appstream/source aarch64/codeready-builder/os aarch64/codeready-builder/debug aarch64/codeready-builder/source"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "openafs")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="openafs/"
    ARCHS="openafs9el-stable/x86_64/debug openafs9el-stable/x86_64/os openafs9el-stable/aarch64/debug openafs9el-stable/aarch64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "eos")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="eos/"
    ARCHS="eos9-stable/x86_64/debug eos9-stable/x86_64/os eos9-stable/aarch64/debug eos9-stable/aarch64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "artifactselastic")
    TOPREPO="mirror/artifacts.elastic.co/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/artifacts.elastic.co/"
    ARCHS="packages/oss-8.x/yum/"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "hashicorp")
    TOPREPO="mirror/rpm.releases.hashicorp.com/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/rpm.releases.hashicorp.com/"
    ARCHS="RHEL/8/x86_64/ RHEL/9/x86_64/"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "authz")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="authz/"
    ARCHS="authz7-stable/x86_64/os authz9el-stable/x86_64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "cuda")
    TOPREPO="mirror/developer.download.nvidia.com/compute/cuda/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/developer.download.nvidia.com/compute/cuda/repos/"
    ARCHS="rhel9/x86_64"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "debian")
    TOPREPO="debian/"
    TOPFILE=$TOPREPO
    TOPDEST="debian/"
    ARCHS="pool" # Always need this
    ARCHS="$ARCHS dists/bookworm dists/bookworm-updates dists/bookworm-backports"
    ARCHS="$ARCHS zzz-dists/bookworm zzz-dists/bookworm-updates zzz-dists/bookworm-backports"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "mongodb")
    TOPREPO="mirror/repo.mongodb.org/yum/redhat/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/repo.mongodb.org/yum/redhat/"
    ARCHS="9/mongodb-org/7.0/x86_64/ 9/mongodb-org/7.0/aarch64/"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "starnet")
    TOPREPO="mirror/www.starnet.com/"
    TOPFILE=$TOPREPO
    TOPDEST="mirror/www.starnet.com/"
    ARCHS="files rpm"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  "monit")
    TOPREPO="internal/repos/"
    TOPFILE=$TOPREPO
    TOPDEST="monit/"
    ARCHS="monit9el-stable/x86_64/os"
    RPREF=""
    REPOS="." # ugly
    SUFF=""
    PROT=""
    ;;

  *)
    echo "Error: unknown comp ($COMP) to snap"
    do_admin_email "yumsnapshots: Error" "I'm really sorry, but I am refusing to snapshot the \"$COMP\" repository as I don't know how to process it.\n\nHave a nice day."
    exit 1
    ;;
  esac

  [ $VERBOSE -eq 1 ] && echo "Starting $(basename $0) at $(date) for $COMP"

  for ARCH in ${ARCHS}; do
    for REPO in ${REPOS}; do
      if [ ! -d "$SPREF/$TOPFILE$ARCH/$SUFF/$REPO/" ]; then
        echo "Source doesn't exist, skipping: $SPREF/$TOPFILE$ARCH/$SUFF/$REPO/"
        continue
      fi
      if [ -d $DPREF/$DATE/$TOPDEST/$ARCH/$REPO ] && [ $FORCE -ne 1 ]; then
        [ $VERBOSE -eq 1 ] && echo "skipping $COMP ($REPO), already there"
      else
        [ $VERBOSE -eq 1 ] && [ $FORCE -eq 1 ] && echo /bin/rm -rf $DPREF/$DATE/$TOPDEST/$ARCH/$REPO
        [ $NOACTION -eq 0 ] && [ $FORCE -eq 1 ] && /bin/rm -rf $DPREF/$DATE/$TOPDEST/$ARCH/$REPO

        [ $VERBOSE -eq 1 ] && echo /bin/mkdir -p $DPREF/$DATE/$TOPDEST/$ARCH/$REPO
        [ $NOACTION -eq 0 ] && /bin/mkdir -p $DPREF/$DATE/$TOPDEST/$ARCH/$REPO

        echo "/bin/cp -al $SPREF/$TOPFILE$ARCH/$SUFF/$REPO/* $DPREF/$DATE/$TOPDEST/$ARCH/"
        [ $NOACTION -eq 0 ] && /bin/cp -al $SPREF/$TOPFILE$ARCH/$SUFF/$REPO/* $DPREF/$DATE/$TOPDEST/$ARCH/

        [ $VERBOSE -eq 1 ] && [ "$PROT" != "" ] && echo /bin/ln -sf ../../../../../$PROT/$ARCH/.htaccess $DPREF/$DATE/$TOPDEST/$ARCH/$REPO/
        [ $NOACTION -eq 0 ] && [ "$PROT" != "" ] && /bin/ln -sf ../../../../../$PROT/$ARCH/.htaccess $DPREF/$DATE/$TOPDEST/$ARCH/$REPO/

      fi
    done
  done

done

[ $VERBOSE -eq 1 ] && echo "Finished `basename $0` at `date`."
