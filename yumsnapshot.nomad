job "${PREFIX}_yumsnapshot" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_yumsnapshot" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/yumsnapshot/yumsnapshot:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_yumsnapshot"
        }
      }
      volumes = [
        "/mnt/data1:/mnt/data1",
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      YUMSNAPSHOT_DESTINATION = "$YUMSNAPSHOT_DESTINATION"
      TAG = "${PREFIX}_yumsnapshot"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 2048 # MB
    }

  }
}
